<?php
include_once('config.php');

// Get link after .com
$fullLink = $_SERVER['REQUEST_URI'];
//echo $fullLink;
// Replace  "/" with " "
$fullLink = str_replace( "/", " ", $fullLink );

// Replace "%20" with " "
$message = str_replace( "%20", " ", $fullLink );

// echo $message;
// Array of commands
$commands = array (
            '@delete',
            '@d',
            '@importance',
            '@i',
            '@task',
            '@t',
            '@message',
            '@m',
            'favicon.ico',
            'symbolmanifest.json'
            );


// Insert New Notes
if ( $message != "" && $message != " " && $message != "  ") {


  $commandCheck = explode(" ", $message);

  // Check for command matches
  if ( in_array($commandCheck[1], $commands )) {

    // @delete or @d command match
    if ( $commandCheck[1] == '@delete' || $commandCheck[1] == '@d' ) {

      if ( $commandCheck[2] == 'all' ) {
        $query = "UPDATE `notetoself`.`notes` SET `active` = '0'";
        mysql_query($query);
      }

      if ( $commandCheck[2] == 'first' || $commandCheck[2] == 'f' ) {
        $query = "UPDATE `notetoself`.`notes` SET `active` = '0' WHERE `active` = '1' ORDER BY `notes`.`time` DESC LIMIT 1";
        mysql_query($query);
      }

      if ( $commandCheck[2] == 'second' || $commandCheck[2] == 's' ) {
        $query = "UPDATE `notetoself`.`notes` SET `active` = '0' WHERE `active` = '1' ORDER BY `notes`.`time` DESC LIMIT 2";
        mysql_query($query);

        $query = "UPDATE `notetoself`.`notes` SET `active` = '1' WHERE `active` = '0' ORDER BY `notes`.`time` DESC LIMIT 1";
        mysql_query($query);
      }

      if ( $commandCheck[2] == 'other' || $commandCheck[2] == '0' ) {
        $query = "UPDATE `notetoself`.`notes` SET `active` = '0'";
        mysql_query($query);

        $query = "UPDATE `notetoself`.`notes` SET `active` = '1' ORDER BY `notes`.`time` DESC LIMIT 1";
        mysql_query($query);
      }

      if ( is_numeric( $commandCheck[2] ) ) {
        $query = "UPDATE `notetoself`.`notes` SET `active` = '0' WHERE `notes`.`noteId` = " . $commandCheck[2];
        mysql_query($query);
      }

    }

    if ( $commandCheck[1] == '@importance' || $commandCheck[1] == '@i' ) {

      if ( $commandCheck[3] == 1 || $commandCheck[3] == 2 || $commandCheck[3] == 3 ) {
        $query = "UPDATE `notetoself`.`notes` SET `importance` = '" . $commandCheck[3] . "' WHERE `noteId` = " . $commandCheck[2];
        mysql_query($query);
      }

      if ( $commandCheck[3] > 3 ) {
        $query = "UPDATE `notetoself`.`notes` SET `importance` = '3' WHERE `noteId` = " . $commandCheck[2];
        mysql_query($query);
      }

      if ( $commandCheck[3] < 0 ) {
        $query = "UPDATE `notetoself`.`notes` SET `importance` = '0' WHERE `noteId` = " . $commandCheck[2];
        mysql_query($query);
      }

    }

    // Allows assigning tasks to other users.
    if ( $commandCheck[1] == '@task' || $commandCheck[1] == '@t' ) {

      $foriegnUser = $commandCheck[2];

      unset( $commandCheck[0], $commandCheck[1], $commandCheck[2] );
      $note = implode( $commandCheck, " " );
      // echo $note;
      // Insert message string into DB
      $query = "INSERT INTO `notetoself`.`notes` (`noteId`, `note`, `user`, `time`, `active`, `type`, `from` ) VALUES (NULL, '" . mysql_real_escape_string( $note ) . "', '" . $foriegnUser . "' , CURRENT_TIMESTAMP, '1', 'noteForeign', '". $fromUsername ."')";
      //echo $query;
      mysql_query($query);

      $note = "Task " . "'" . $note . "' sent to " . $foriegnUser . ".";
      $query = "INSERT INTO `notetoself`.`notes` (`noteId`, `note`, `user`, `time`, `active`, `type`, `from` ) VALUES (NULL, '" . mysql_real_escape_string( $note ) . "', '" . $user . "' , CURRENT_TIMESTAMP, '1', 'message', '". $user ."')";
      //echo $query;
      mysql_query($query);


    }

    if ( $commandCheck[1] == '@message' || $commandCheck[1] == '@m' ) {

      $foriegnUser = $commandCheck[2];

      unset( $commandCheck[0], $commandCheck[1], $commandCheck[2] );
      $note = implode( $commandCheck, " " );
      // echo $note;
      // Insert message string into DB
      $query = "INSERT INTO `notetoself`.`notes` (`noteId`, `note`, `user`, `time`, `active`, `type`, `from` ) VALUES (NULL, '" . mysql_real_escape_string( $note ) . "', '" . $foriegnUser . "' , CURRENT_TIMESTAMP, '1', 'message', '". $fromUsername ."')";
      //echo $query;
      mysql_query($query);

      $note = "Message " . "'" . $note . "' sent to " . $foriegnUser . ".";
      $query = "INSERT INTO `notetoself`.`notes` (`noteId`, `note`, `user`, `time`, `active`, `type`, `from` ) VALUES (NULL, '" . mysql_real_escape_string( $note ) . "', '" . $user . "' , CURRENT_TIMESTAMP, '1', 'message', '". $user ."')";
      //echo $query;
      mysql_query($query);

    }


  } else {

    // Insert message string into DB
    $query = "INSERT INTO `notetoself`.`notes` (`noteId`, `note`, `user`, `time`, `active`, `type`, `from` ) VALUES (NULL, '" . mysql_real_escape_string( $message ) . "', '" . $user . "' , CURRENT_TIMESTAMP, '1', 'note', '')";
//  echo $query;
    mysql_query($query);
  }
}

  // Call all active notes.
  $notes = array();
  $query = "SELECT * FROM `notes` WHERE `active` = 1 AND `type` != 'message' AND `user` = '" . $user . "' ORDER BY `notes`.`time` DESC ";
  $result = mysql_query($query);
  while($row = mysql_fetch_array($result)){
      $notes[] = $row;

}

  // Get Messages
  $message = array();
  $query = "SELECT * FROM `notes` WHERE `active` = 1 AND `type` = 'message' AND `user` = '" . $user . "' ORDER BY `notes`.`time` DESC";
  $result = mysql_query($query);
  while($row = mysql_fetch_array($result)){
      $messages[] = $row;

}

$messageCount = count($messages);

?>

<html lang="en">
	<head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="author" content="Tracey Boyd, Project Director Kotebo">

	<title>

    Note To Self!

	</title>

  <link rel="stylesheet" type="text/css" href="styles.css">

</head>
<body>
<?php

  echo "<div id='container'>";

  if ( $messageCount > 0 ) {



    $i = 0;

      foreach( $messages as $note ) {

        if ( $note['from'] != "" ) {
          $messageFrom = "<span id='fromUserMessage' > - " . $note['from'] . "</span>" ;
        }

        $lowerFrom = strtolower( $note['from'] );
        $lowerUser = strtolower( $note['user'] );

        if ( $lowerUser == $lowerFrom ) {
          $messageFrom = "" ;
        }

        while( $i < 1 ){
        echo "<div class='message' alt='". $note['time'] ."'><a class='messageCount' href='/@delete%20" . $note['noteId'] . "'>" . $messageCount . "</a><span id='messageContent'>".$note['note']. $messageFrom . "</span></div>";
        $i++;
        }

    }

  }

  echo "<div id='user' >" . $userMessage . "</div>";

  $i = 0;
  foreach( $notes as $note ) {

    if ( $note['from'] != "" ) {
      $from = "<span id='fromUser' > - " . $note['from'] . "</span>" ;
    }

    if ( $i == 0 ){
      echo "<div id='noteHeader'><div class='note' alt='". $note['time'] ."'><a class='noteId' href='/@delete%20" . $note['noteId'] . "'>". $note['noteId']. "</a><span class='importance-". $note['importance'] ."' > ".$note['note']. $from . "</span></div></div><div id='noteList'>";
    } else {
      echo "<div class='note' alt='". $note['time'] ."'><a class='noteId' href='/@delete%20" . $note['noteId'] . "'>". $note['noteId']. "</a><span class='importance-". $note['importance'] ."' > ".$note['note']. $from . "</span></div>";
    }

  $from = "";
  $i++;

  }

  echo "</div></div></div>";

?>
</body>
</html>
