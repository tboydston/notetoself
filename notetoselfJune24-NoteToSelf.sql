-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 24, 2014 at 02:23 AM
-- Server version: 5.6.17
-- PHP Version: 5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `notetoself`
--

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

CREATE TABLE IF NOT EXISTS `notes` (
  `noteId` int(11) NOT NULL AUTO_INCREMENT,
  `note` text NOT NULL,
  `user` varchar(55) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` tinyint(4) NOT NULL,
  `type` varchar(55) NOT NULL,
  `from` varchar(55) NOT NULL,
  `importance` tinyint(4) NOT NULL DEFAULT '0',
  UNIQUE KEY `noteId_2` (`noteId`),
  KEY `noteId` (`noteId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=84 ;

--
-- Dumping data for table `notes`
--

INSERT INTO `notes` (`noteId`, `note`, `user`, `time`, `active`, `type`, `from`, `importance`) VALUES
(78, ' test', 'tracey', '2014-06-23 16:46:22', 0, 'note', '', 0),
(79, ' This is a test', 'tracey', '2014-06-23 16:46:29', 1, 'note', '', 0),
(80, 'test', 'rannie', '2014-06-23 16:46:41', 1, 'message', 'Tracey', 0),
(81, 'Message ''test'' sent to rannie.', 'tracey', '2014-06-23 16:46:46', 0, 'message', 'tracey', 0),
(82, 'test', 'rannie', '2014-06-23 17:00:24', 1, 'message', 'Tracey', 0),
(83, 'Message ''test'' sent to rannie.', 'tracey', '2014-06-23 17:00:24', 1, 'message', 'tracey', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
